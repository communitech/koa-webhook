'use strict';

var _ = require('lodash');

function Service(name, callbacks) {
  var service = this;

  service.name = name;
  service.callbacks = callbacks;

  return service;
};

Service.prototype.match = function(request) {
  var service = this;

  service.path = null;

  return service.callbacks.match.call(this, request);
};

Service.prototype.validate = function(request) {
  var service = this;

  return service.callbacks.validate.call(this, request);
};

module.exports = Service;