'use strict';

var _ = require('lodash');
var compose = require('koa-compose');
var Service = require('./service');

function Webhooks(app, opts) {
  if (!(this instanceof Webhooks)) {
    if (app) {
      var webhooks = new Webhooks(app, opts);
      return webhooks.middleware();
    } else {
      return new Webhooks(app, opts);
    }
  }

  this.stack = {
    'service': [],
    'webhook': {},
    'middleware': []
  };

  if (app) {
    this.extendApp(app);
  }
};

Webhooks.prototype.version = '1.0.0';

Webhooks.prototype.service = function (name, callbacks) {
  var service = new Service(name, callbacks);
  this.stack.service.push(service);
  return service;
};

Webhooks.prototype.webhook = function(path, middleware) {
  this.stack.webhook[path] = this.stack.webhook[path] || [];
  this.stack.webhook[path].push(middleware);
};

Webhooks.prototype.middleware = function middleware() {
  var webhooks = this;
  var middleware = function * dispatch(next) {
    var service = webhooks.match(this.request);

    if (service) {
      if (_.has(webhooks.stack.webhook, service.path)) {
        return yield * compose(webhooks.stack.webhook[service.path]).call(this, next);
      } else {
        this.status = 501;
      }
    }

    return yield * next;
  };

  if (webhooks.stack.middleware.length) {
    return compose(webhooks.stack.middleware.concat(middleware));
  } else {
    return middleware;
  }
};

Webhooks.prototype.match = function (request) {
  var services = this.stack.service;

  for (var len = services.length, i = 0; i < len; i++) {
    if (services[i].match(request)) {
      return services[i];
    }
  }

  return false;
};

Webhooks.prototype.extendApp = function (app) {
  var webhooks = this;

  app.webhook = webhooks.webhook;
  app.webhooks = webhooks;

  return app;
};

module.exports = Webhooks;