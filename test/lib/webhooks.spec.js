'use strict';

var _ = require('lodash');
var chai = require('chai')
var expect = chai.expect;
var koa = require('koa');
var http = require('http');
var request = require('supertest');
var koaBodyParser = require('koa-bodyparser');

var Webhooks = require('../../lib/webhooks');
var Service = require('../../lib/service');

describe('Webhooks', function() {
  it('creates new webhooks with koa app', function(done) {
    var app = koa();
    var webhooks = new Webhooks(app);

    expect(webhooks).to.be.instanceOf(Webhooks);

    done();
  });

  it('exposes middleware factory', function(done) {
    var app = koa();

    var webhooks = new Webhooks(app);
    expect(webhooks).to.have.property('middleware');
    expect(webhooks.middleware).to.be.a('function');

    var middleware = webhooks.middleware();
    expect(middleware).to.exist;
    expect(middleware).to.be.a('function');

    done();
  });

  it('should add a service', function (done) {
    var app = koa();
    var webhooks = new Webhooks(app);

    var service = webhooks.service('github', {
      match: function matchGithubWebhook(request) {},
      validate: function validateGithubWebhook(request) {}
    });

    expect(service).to.exist;
    expect(service).to.be.instanceOf(Service);
    expect(service.match).to.be.a('function');
    expect(service.validate).to.be.a('function');

    done();
  });

  it('should add a webhook', function (done) {
    var app = koa();
    var webhooks = new Webhooks(app);

    var webhook = webhooks.webhook('github.issues.opened', function * githubIssuesOpenedWebhook(next) {
      yield * next;
    });

    done();
  });

  it('should match a service', function (done) {
    var app = koa();
    var webhooks = new Webhooks(app);

    app.use(webhooks.middleware());

    // Add a mock Github service...
    webhooks.service('github', {
      match: function matchGithubWebhook(request) {
        if (request.path == '/webhook') {
          if (_.has(request.header, 'x-github-event')) {
            this.path = 'github.' + request.header['x-github-event'] + '.opened';
            return true;
          }

          return false;
        }

        return false;
      }
    });

    var server = http.createServer(app.callback());

    request(server)
      .post('/webhook')
      .set('X-Github-Event', 'issues')
      .set('X-Github-Delivery', '72d3162e-cc78-11e3-81ab-4c9367dc0958')
      .expect(501)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }

        done();
      })
    ;
  });

  it('should match a service and webhook', function (done) {
    var app = koa();
    var webhooks = new Webhooks(app);

    app.use(koaBodyParser());
    app.use(webhooks.middleware());

    // Add a mock Github service...
    webhooks.service('github', {
      match: function matchGithubWebhook(request) {
        if (request.path == '/webhook') {
          if (_.has(request.header, 'x-github-event')) {
            this.path = 'github.' + request.header['x-github-event'] + '.' + request.body.action;
            return true;
          }

          return false;
        }

        return false;
      }
    });

    // Add a mock webhook
    webhooks.webhook('github.issues.opened', function * githubIssuesOpenedWebhook(next) {
      this.status = 200;
      yield * next;
    });

    var server = http.createServer(app.callback());

    request(server)
      .post('/webhook')
      .send({
        action: 'opened'
      })
      .set('X-Github-Event', 'issues')
      .set('X-Github-Delivery', '72d3162e-cc78-11e3-81ab-4c9367dc0958')
      .expect(200)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }

        done();
      });
  });
});