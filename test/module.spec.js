'use strict';

var chai = require('chai')
var expect = chai.expect;

describe('module', function () {
  it('must expose Webhooks', function (done) {
    var Webhooks = require('..');
    expect(Webhooks).to.exist;
    expect(Webhooks).to.be.a('function');
    done();
  });

  it('must expose version number', function (done) {
    var webhooks = require('..')();
    expect(webhooks).to.have.property('version');
    expect(webhooks.version).to.match(/[0-9]+\.[0-9]+\.[0-9]+/);
    done();
  });
});